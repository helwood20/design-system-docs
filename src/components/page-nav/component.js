module.exports = {
	onInput: function(input) {
		input.anchors = input.anchors || ["Overview"];
	},
	onMount: function() {


		var pageNavPosTop = $('.page-nav').offset().top;
		var pageNavPosLeft = $('.page-nav').offset().left;
		// var menuItems


		$('.main').on('scroll', () => {
			var windowPos = $('.main').scrollTop();
			
			if(windowPos >= pageNavPosTop) {
				$('.page-nav').addClass('fixed');
				$('.page-nav').css('left',pageNavPosLeft);
			} else {
				$('.page-nav').removeClass('fixed');
				$('.page-nav').css('left','0');
			}
		});

		$('.main').scrollspy({ target: '.page-nav'}); 
	}
}
const Highlights = require('highlights');
const highlighter = new Highlights({ scopePrefix: 'syntax--' });
const redent = require('redent');

highlighter.requireGrammarsSync({
  modulePath: require.resolve('language-css/package.json')
});

highlighter.requireGrammarsSync({
  modulePath: require.resolve('language-javascript/package.json')
});

highlighter.requireGrammarsSync({
  modulePath: require.resolve('language-html/package.json')
});

highlighter.requireGrammarsSync({
  modulePath: require.resolve('language-marko/package.json')
});

highlighter.requireGrammarsSync({
  modulePath: require.resolve('react/package.json')
});

highlighter.requireGrammarsSync({
  modulePath: require.resolve('language-shellscript/package.json')
});

module.exports = function (input, out) {
  let lang = input.lang;
  let code = input.code || input;

  if (code.renderBody) {
    let codeOut = out.createOut();
    codeOut.sync();
    code.renderBody(codeOut);
    code = codeOut.toString();
  }

  let scopeName;

  if (lang === 'js' || lang === 'javascript' || lang === 'json') {
    scopeName = 'source.js';
  } else if (lang === 'css') {
    scopeName = 'source.css';
  } else if (lang === 'html') {
    scopeName = 'text.html.basic';
  } else if (lang === 'xml' || lang === 'marko') {
    scopeName = 'text.marko';
  } else if (lang === 'jsx') {
    scopeName = 'source.js.jsx';
  } else if (lang === 'bash') {
    scopeName = 'source.shell';
  }

  code = redent(code.replace(/\t/g, '  ')).trim();

  return highlighter.highlightSync({
    fileContents: code,
    scopeName: scopeName
  });
};

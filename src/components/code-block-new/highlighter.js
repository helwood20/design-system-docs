const Highlights = require('highlights');
const highlighter = new Highlights({ scopePrefix: 'syntax--' });

// highlighter.requireGrammarsSync({
//     modulePath: require.resolve('language-css/package.json')
// });
// highlighter.requireGrammarsSync({
//     modulePath: require.resolve('language-javascript/package.json')
// });
// highlighter.requireGrammarsSync({
//     modulePath: require.resolve('language-html/package.json')
// });
highlighter.requireGrammarsSync({
    modulePath: require.resolve('language-marko/package.json')
});

module.exports = highlighter;
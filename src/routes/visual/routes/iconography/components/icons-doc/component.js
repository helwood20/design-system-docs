module.exports = {
	onInput(input) {
	    this.state = {
			code: '<i class="icon-lowes-fill"></i>'
	    };
	},
	handleInputKeyUp( event, el ) {
		if ( event.keyCode == 13 ) { return false; }
		var iconFilterField = this.getEl('filter-icons').value;
		var filteredIcons = document.getElementById('iconListFiltered');
		filteredIcons.innerHTML = '';
		
    if( /^-?.+$/.test( iconFilterField ) ) {
	    var everyChild = document.querySelectorAll("#global_icon_container .icon-container");		
			everyChild.forEach( function( iconContainerId ) {				
				if ( iconContainerId.id.search( iconFilterField ) > 0) {
					iconContainerId.style.display = "block";
						
					filteredIcons.innerHTML = filteredIcons.innerHTML + "<div class='grid-20 tabet-grid-20 mobile-grid-50 icon-container'>" + iconContainerId.innerHTML + "</div>";
				} else {
					iconContainerId.style.display = "none";
				}
				
				iconContainerId.style.display = "none";
			});
    } else if ( iconFilterField == '' ) {
	    var everyChild = document.querySelectorAll("#global_icon_container .icon-container");		
			
			everyChild.forEach( function( iconContainerId ) {
				iconContainerId.style.display = "block";
			});
    }
		event.preventDefault();
  },
  handleIconPanel( icon_class, panel_number ) {
	    
	  var iconContainer = document.getElementById( icon_class + "_main_container" ).querySelector( ".trench" );
		var selectedArrow = document.getElementById( icon_class + "_main_container" ).querySelector( ".selected-arrow" );
		var Panel = document.getElementById( "icon_panel_" + panel_number );
		
		var iconNorm = document.getElementById("iconSizeVariantNorm"+panel_number);
		var icon15 = document.getElementById("iconSizeVariant15"+panel_number);
		var icon2 = document.getElementById("iconSizeVariant2"+panel_number);
		var icon25 = document.getElementById("iconSizeVariant25"+panel_number);
		var icon3 = document.getElementById("iconSizeVariant3"+panel_number);
		var icon35 = document.getElementById("iconSizeVariant35"+panel_number);
		var icon4 = document.getElementById("iconSizeVariant4"+panel_number);
		var icon45 = document.getElementById("iconSizeVariant45"+panel_number);
		var icon5 = document.getElementById("iconSizeVariant5"+panel_number);
		
		var iconTitleName = document.getElementById("iconTitle"+panel_number);
		
		var classToRemove = document.getElementById("selectedIcon"+panel_number).value;
		if ( classToRemove !== '' ) {
			iconNorm.classList.remove( classToRemove );
			icon15.classList.remove( classToRemove );
			icon2.classList.remove( classToRemove );
			icon25.classList.remove( classToRemove );
			icon3.classList.remove( classToRemove );
			icon35.classList.remove( classToRemove );
			icon4.classList.remove( classToRemove );
			icon45.classList.remove( classToRemove );
			icon5.classList.remove( classToRemove );
			iconTitleName.innerHTML = "";
		}
		
		document.getElementById("selectedIcon"+panel_number).value = icon_class;
		
		//If the element isn't already selected, deactivate any selected elements and activate the new one.	
			if ( !iconContainer.classList.contains( "selected-icon" ) ) {
				
				//Remove all other selected object identifiers before applying new one
				var allTrenches = document.getElementsByClassName( "trench-for-icons" );
				var allArrows = document.getElementsByClassName( "selected-arrow" );
				var allPanels = document.getElementsByClassName( "icon-panel" );
				
				for ( var i = 0; i < allTrenches.length; i++ ) {
				    allTrenches[i].classList.remove( "selected-icon" );
				}
				
				for ( var i = 0; i < allArrows.length; i++ ) {
				    allArrows[i].classList.remove( "on" );
				}
				
				for ( var i = 0; i < allPanels.length; i++ ) {
				    allPanels[i].classList.add( "panel-hidden" );
				}
				
				//Add new selection states
				iconContainer.classList.add( "selected-icon" );
				selectedArrow.classList.add( "on" );
				
				var classToAdd = document.getElementById("selectedIcon"+panel_number).value;
				
				iconNorm.classList.add( classToAdd );
				icon15.classList.add( classToAdd );
				icon2.classList.add( classToAdd );
				icon25.classList.add( classToAdd );
				icon3.classList.add( classToAdd );
				icon35.classList.add( classToAdd );
				icon4.classList.add( classToAdd );
				icon45.classList.add( classToAdd );
				icon5.classList.add( classToAdd );
				iconTitleName.innerHTML = classToAdd;
			    			
				Panel.classList.remove( "panel-hidden" );
				console.log("#iconCodeOutput"+panel_number);
				
				var codeBlock = document.getElementById('iconCodeOutput'+panel_number).innerHTML = '<code-block lang="html"><i class="'+classToAdd+'"></i></code-block>';
				
			//Else it already has one and we're just disabling it.
			} else {
				iconContainer.classList.remove( "selected-icon" );
				selectedArrow.classList.remove( "on" );
				Panel.classList.add( "panel-hidden" );
			}
    },
    closeIconPanel( panel_number ) {
			
			//Remove all selected object identifiers before applying new one
			var allTrenches = document.getElementsByClassName( "trench-for-icons" );
			var allArrows = document.getElementsByClassName( "selected-arrow" );
			var Panel = document.getElementById( "icon_panel_" + panel_number );
			
			for ( var i = 0; i < allTrenches.length; i++ ) {
			    allTrenches[i].classList.remove( "selected-icon" );
			}
			
			for ( var i = 0; i < allArrows.length; i++ ) {
			    allArrows[i].classList.remove( "on" );
			}
			
			Panel.classList.add( "panel-hidden" );
		
    },
    handleSelectedIcon( panel_number, icon_variant ) {
	    
	    var allSelected = document.getElementsByClassName( "selected" );
	    for ( var i = 0; i < allSelected.length; i++ ) {
			    allSelected[i].classList.remove( "selected" );
			}
			var selected_icon = document.getElementById(icon_variant+"container"+panel_number);
			selected_icon.classList.add('selected');
			
			var icon_Selected = document.getElementById(icon_variant+panel_number);
			console.log(icon_Selected);
			var classToAdd = document.getElementById("selectedIcon"+panel_number).value;
	    var codeBlock = document.getElementById('iconCodeOutput'+panel_number).innerHTML = '<code-block lang="html"><i class="'+classToAdd+'"></i></code-block>';
	    
	}
}